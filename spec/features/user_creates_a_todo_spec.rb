require 'rails_helper'

RSpec.feature "UserCreatesATodos", type: :feature do
  scenario "successfully" do
    #user signs in
    sign_in_as("user@example.com")

    #user creates a to-do via a form
    create_todo("paint house")

    #verify that the page has our to-do
    expect(page).to have_css ".todos li", text: "paint house"
  end
end
