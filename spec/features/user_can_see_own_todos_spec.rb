require 'rails_helper'

RSpec.feature "UserCanSeeOwnTodos", type: :feature do
  scenario "successfully" do
    #create different user's to-do in background
    Todo.create(title: "buy milk", email: "somebody@example.com")

    #user signs in
    sign_in_as("user@example.com")

    #user creates a to-do via a form
    create_todo("paint house")

    #verify that the page has the signed in user's to-do, but not the to-do created in the background
    expect(page).to have_css ".todos li", text: "paint house"
    expect(page).not_to have_css ".todos li", text: "buy milk"
  end
end
