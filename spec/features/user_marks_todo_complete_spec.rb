require 'rails_helper'

RSpec.feature "UserMarksTodoComplete", type: :feature do
  scenario "successfully" do
    #user signs in
    sign_in_as("user@example.com")

    #user creates a to-do via a form
    create_todo("paint house")

    #user marks the to-do complete
    click_on "Complete"

    #verify that the to-do is marked complete
    expect(page).to have_css ".todos li.completed", text: "paint house"
  end
end
