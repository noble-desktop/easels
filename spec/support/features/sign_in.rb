module Features
  def sign_in_as(email)
    visit new_session_path
    fill_in "Email", with: email
    click_on "Sign In"
  end

  def create_todo(title)
    click_on "Add a New To-Do"
    fill_in "Title", with: title
    click_on "Add To-Do"
  end
end
