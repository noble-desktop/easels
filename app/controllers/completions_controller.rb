class CompletionsController < ApplicationController
  before_action :authenticate!
  before_action :get_todo

  def create
    mark_todo(Time.now)
    redirect_to root_path
  end

  def destroy
    mark_todo(nil)
    redirect_to root_path
  end

  private
    def get_todo
      @todo = Todo.find(params[:todo_id])
    end

    def mark_todo(time)
      @todo.completed_at = time
      @todo.save
    end
end
