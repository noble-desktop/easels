class ApplicationController < ActionController::Base
  before_action :set_current_email

  private
    def set_current_email
      @current_email = session[:current_email]
    end

    def authenticate!
      redirect_to new_session_path unless @current_email
    end
end
